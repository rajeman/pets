package com.rajeman.com.pets;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import static android.R.id.edit;
import static java.security.AccessController.getContext;

public class CatalogActivity extends AppCompatActivity {
    ListView petsActivityListView;
    DatabaseAdapter databaseAdapter;
    Cursor databaseCursor;
    SQLiteDatabase db;
   public static final String[] tableColumns = {
            FeedReaderContract.FeedEntry._ID,
            FeedReaderContract.FeedEntry.COLUMN_NAME_NAME,
            FeedReaderContract.FeedEntry.COLUMN_NAME_BREED,
            FeedReaderContract.FeedEntry.COLUMN_NAME_GENDER,
            FeedReaderContract.FeedEntry.COLUMN_NAME_WEIGHT
    };

    FeedReaderHelper feedReaderHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        Toolbar mainActivityToolbar = (Toolbar) findViewById(R.id.pets_activity_toolbar);
        setSupportActionBar(mainActivityToolbar);
        petsActivityListView = (ListView) findViewById(R.id.pets_activity_list_view);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.new_pet_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAddPetsActivity();
            }
        });

        feedReaderHelper = new FeedReaderHelper(this);
        db = feedReaderHelper.getReadableDatabase();
        databaseCursor = db.query(FeedReaderContract.FeedEntry.TABLE_NAME, tableColumns, null, null, null, null, null);

        databaseAdapter = new DatabaseAdapter(this, databaseCursor, 0);
        petsActivityListView.setAdapter(databaseAdapter);
        petsActivityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Cursor cursor = (Cursor)parent.getItemAtPosition(position);
                int currentPosition = cursor.getPosition();
                editItemDetails(currentPosition);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.insert_dummy_menu:
                insertDummyData();
            default:
                return super.onOptionsItemSelected(item);

        }


    }


    private void insertDummyData() {

        SQLiteDatabase db = feedReaderHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_NAME, getString(R.string.dummy_name));
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_BREED, getString(R.string.dummy_breed));
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_GENDER, 1);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_WEIGHT, 48);
        long newRowId = db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);
        if (newRowId > -1) {
            updateView();
            Toast.makeText(this, getString(R.string.toast_dummy_insertion_success) + " " + newRowId, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.toast_dummy_insertion_failed), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {
        feedReaderHelper.close();
        super.onDestroy();
    }

    public void updateView() {

        databaseAdapter.changeCursor(db.query(FeedReaderContract.FeedEntry.TABLE_NAME, tableColumns, null, null, null, null, null));
        databaseAdapter.notifyDataSetChanged();
    }
    protected void launchAddPetsActivity(){
        Intent intent = new Intent(this, AddPetsActivity.class);
        startActivity(intent);
    }
    protected void editItemDetails(int currentPosition){

        Intent intent = new Intent(this, EditPetActivity.class);
        intent.putExtra(getString(R.string.cursor_position), currentPosition);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateView();
    }

}
