package com.rajeman.com.pets;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import static com.rajeman.com.pets.CatalogActivity.tableColumns;

public class EditPetActivity extends AppCompatActivity {
    Cursor c;
    SQLiteDatabase db;
    int currentId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);

        Toolbar mainActivityToolbar = (Toolbar) findViewById(R.id.add_pets_activity_toolbar);
        mainActivityToolbar.setTitle(getString(R.string.edit_pet_activity_title));
        setSupportActionBar(mainActivityToolbar);

        Intent intent = getIntent();
        int cursorPosition = intent.getIntExtra(getString(R.string.cursor_position), -1 );
        FeedReaderHelper feedReaderHelper = new FeedReaderHelper(this);
        db = feedReaderHelper.getReadableDatabase();
         c = db.query(FeedReaderContract.FeedEntry.TABLE_NAME, tableColumns, null, null, null, null, null);
        c.moveToPosition(cursorPosition);
        EditText nameEditText = (EditText)findViewById(R.id.name_edit_text);
        EditText breedEditText = (EditText)findViewById(R.id.breed_edit_text);
        EditText weightEditText = (EditText)findViewById(R.id.weight_edit_text);
        Spinner genderSpinner = (Spinner)findViewById(R.id.gender_select_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gender_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        genderSpinner.setAdapter(adapter);
        nameEditText.setHint(c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_NAME)));
        breedEditText.setHint(c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_BREED)));
        weightEditText.setHint(c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_WEIGHT)));
        genderSpinner.setSelection(c.getInt(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_GENDER)) - 1);
        currentId = c.getInt(c.getColumnIndex(FeedReaderContract.FeedEntry._ID));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pet_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_menu:
                deleteCurrentPet();
            default:
                return super.onOptionsItemSelected(item);

        }


    }

    protected void deleteCurrentPet(){

        int isDeleted = db.delete(FeedReaderContract.FeedEntry.TABLE_NAME, FeedReaderContract.deleteItem()+ currentId, null);
        if(isDeleted ==1){

            Toast.makeText(getBaseContext(), getString(R.string.toast_delete_successful), Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getBaseContext(), getString(R.string.toast_delete_failed), Toast.LENGTH_SHORT).show();
        }
        finish();

    }
}
