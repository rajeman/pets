package com.rajeman.com.pets;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import static com.rajeman.com.pets.R.string.gender;
import static java.lang.Integer.parseInt;

public class AddPetsActivity extends AppCompatActivity {
   private Spinner genderSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);
        Toolbar mainActivityToolbar = (Toolbar) findViewById(R.id.add_pets_activity_toolbar);
        mainActivityToolbar.setTitle(getString(R.string.add_pets_activity_title));
        setSupportActionBar(mainActivityToolbar);

         genderSpinner = (Spinner) findViewById(R.id.gender_select_spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gender_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        genderSpinner.setAdapter(adapter);

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                              @Override
                                              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                              }

                                              @Override
                                              public void onNothingSelected(AdapterView<?> parent) {

                                              }
                                          }
        );
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addpet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save_pet_menu:
                savePet();
            default:
                return super.onOptionsItemSelected(item);

        }


    }

    protected void savePet(){

        EditText nameEditText = (EditText)findViewById(R.id.name_edit_text);
        EditText breedEditText = (EditText)findViewById(R.id.breed_edit_text);
        EditText weightEditText = (EditText)findViewById(R.id.weight_edit_text);
        String name = nameEditText.getText().toString();
        name = name.trim();
        String breed = breedEditText.getText().toString();
        breed = breed.trim();
        String weightText = weightEditText.getText().toString().trim();
        int  weight = -1;
        if(weightText.length()>0){

           weight =  Integer.parseInt(weightText);

        }

        if(name.length()>0 && breed.length()>0 && weight >0){
        int gender = genderSpinner.getSelectedItemPosition() + 1;

        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_NAME, name);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_BREED, breed);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_GENDER, gender);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_WEIGHT, weight);
        FeedReaderHelper feedReaderHelper = new FeedReaderHelper(this);
        SQLiteDatabase db = feedReaderHelper.getWritableDatabase();
        long newRowId = db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);
        if(newRowId>-1){

            Toast.makeText(getBaseContext(), getString(R.string.save_successful), Toast.LENGTH_SHORT).show();
            finish();
        }
        }

        else{
            Toast.makeText(getBaseContext(), getString(R.string.fill_all_fields), Toast.LENGTH_SHORT).show();
        }

    }
}
