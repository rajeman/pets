package com.rajeman.com.pets;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ROJO on 22-Apr-18.
 */

public class DatabaseAdapter extends CursorAdapter {

    FeedReaderContract feedReaderContract;

    public DatabaseAdapter (Context context, Cursor c, int flags){
        super(context, c, flags);

    }
    @Override

    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(R.layout.pets_activity_list_view_template,parent,false);
        //return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        int petNameColumnIndex = cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_NAME);
        String petName = cursor.getString(petNameColumnIndex);
        String petBreed = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_BREED));
        TextView petNameTextView = (TextView)view.findViewById(R.id.pet_name_txt_view);
        TextView petBreedTextView = (TextView)view.findViewById(R.id.pet_breed_txt_view);
        petNameTextView.setText(petName);
        petBreedTextView.setText(petBreed);


    }
}
