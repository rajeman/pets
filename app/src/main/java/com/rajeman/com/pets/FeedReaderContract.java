package com.rajeman.com.pets;

import android.provider.BaseColumns;

import static android.R.attr.id;

/**
 * Created by ROJO on 22-Apr-18.
 */

public final class FeedReaderContract {

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}

    /* Inner class that defines the table contents */
     public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "pets";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_BREED = "breed";
        public static final String COLUMN_NAME_GENDER = "gender";
        public static final String COLUMN_NAME_WEIGHT = "WEIGHT";

    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    FeedEntry.COLUMN_NAME_NAME + " TEXT," +
                    FeedEntry.COLUMN_NAME_BREED + " TEXT," +
                    FeedEntry.COLUMN_NAME_GENDER + " INTEGER," +
                    FeedEntry.COLUMN_NAME_WEIGHT + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    private static final String SQL_DELETE_ITEM = FeedEntry._ID +" = ";

    public static String createTable(){

        return SQL_CREATE_ENTRIES;
    }

    public static String deleteTable(){

        return SQL_DELETE_ENTRIES;
    }
    public static String  deleteItem(){

        return SQL_DELETE_ITEM;

    }
}
